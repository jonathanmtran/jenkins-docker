# Jenkins Docker Image

This Docker image takes the `jenkins/jenkins` image and adds the following:

- MinIO Client (mc)
- Node.js LTS
- Python 3 and python-requests

## Usage

See https://github.com/jenkinsci/docker/blob/master/README.md for usage
information.
