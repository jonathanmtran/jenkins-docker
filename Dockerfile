ARG JENKINS_VERSION=latest

FROM jenkins/jenkins:${JENKINS_VERSION}

# if we want to install via apt
USER root

RUN export DEBIAN_FRONTEND=noninteractive && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt install -y build-essential nodejs python3-requests && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*

RUN cd /usr/local/bin && \
    wget https://dl.min.io/client/mc/release/linux-amd64/mc && \
    chmod +x mc

# drop back to the regular jenkins user - good practice
USER jenkins
